# Django Example

Django 공부용



## Bookmark app

북마크에 등록된 URL을 따라 다른 사이트로 이동하는 링크 기능을구현해볼 수 있고, 북마크의 생성, 수정, 삭제 등의 기능을 구현



### 작업 순서

1. 뼈대 만들기
   1. 관련 파일 - settings.py
   2. 관련 명령 - startproject, migrate, createsuperuser, startapp
2. 모델 코딩
   1. 관련 파일 - models.py, admin.py
   2. 관련 명령 - makemigrations, migrate
3. URLconf 코딩
   1. 관련 파일 - urls.py
4. 뷰 코딩
   1. 관련 파일 - views.py
5. 템플릿 코딩
   1. 관련 파일 - templates 디렉터리


> 기본 테이블 설정
>
> 모델을 설정하기 전에 migrate 명령을 해라!!
>
> 이유 : Django는 모든 웹 프로젝트 개발 시 사용자와 사용자의 권한 그룹 테이블이 반드시 필요하는 가정 하에 설계되었다.
>
> 테이블(모델)을 만들지 않더라도, 사용자 및 권한 그룹 테이블을 만들어 주기 위해 프로젝트 개발 시작 시점에 이 명령을 실행
>
> 결과 : db.sqlite3 파일이 생성



> 슈퍼 유저 생성
>
> ID = asvv PW= qwer1234



## Blog app

블로그는 웹(web) 로그(log)의 줄임말로 '웹 상에 기록하는 일지'이다.

글 등록 및 열람, 태그 달기, 댓글 및 검색 기능, 콘텐츠 생성 및 편집 기능 구현



### 작업 순서

1. 뼈대 만들기
   1. 관련 파일 - settings.py
   2. 관련 명령 - migrate, startapp
2. 모델 코딩
   1. 관련 파일 - models.py, admin.py
   2. 관련 명령 - makemigrations, migrate
3. URLconf 코딩
   1. 관련 파일 - urls.py
4. 뷰 코딩
   1. 관련 파일 - views.py
5. 템플릿 코딩
   1. 관련 파일 - templates 디렉터리




## Home page 

처음으로 보여주는 페이지를 홈헤이지라고 한다. 기능 보다는 디자인 측면이 중요하기 때문에 HTML, Javascript, CSS 등의 지식이 필요한 분야이다.



### 작업 순서

1. 뼈대 만들기
   1. 관련 파일 - settings.py
   2. 관련 명령 - migrate
2. 모델 코딩
   1. 관련 파일 - models.py, admin.py
   2. 관련 명령 - makemigrations, migrate
3. URLConf 코딩
   1. 관련 파일 - urls.py
4. 뷰 코딩
   1. 관련 파일 - views.py
5. 템플릿 코딩
   1. 관련 파일 - templates 디렉터리

## Tagging

> pip install django-tagging

## 댓글

> pip install disqus

## 검색 기능
검색 기능은 블로그 앱 내에서의 검색 기능을 구현하는 것으로 장고 자체의 Q-객체를 이용하면 어렵지 않게 구현할 수 있다.
Q-객체는 테이블에 대한 복잡한 쿼리를 처리하기 위한 객체이다.

1. URLConf - /blog/search/ 추가

## Photo 앱

1. 뼈대 만들기
    1. 관련 파일 - settings.py
    2. 관련 명령 - startapp
2. 모델 코딩
    1. 관련 파일 - models.py, admin.py, fields.py
    2. 관련 명령 - makemigrations, migrate
3. URLConf
    1. 관련 파일 - urls.py
4. 뷰 코딩
    1. 관련 파일 - views.py
5. 템블릿 코딩하기
    1. 관련 파일 - templates 디렉토리
6. 그외
    1. 관련 파일 - static 디렉터리(사진 정렬을 위한 photo.css 추가)

## 인증 기능
웹 개발 시 필수 기능인 인증 기능을 개발
장고 엔진 내부에서는 웹 요청 및 사용자 식별 사용자별 세션 할당 및 관리 기능도 수행되는데, 이런 세션 처리도 인증 기능에 포함된다.

### Application 설계하기
장고 패키지에 포함되어 있는 django.contrib.auth 앱이 인증 기능을 담당한다.

