from django.contrib import admin
from bookmark.models import Bookmark

# Register your models here.

class BookmarkAdmin(admin.ModelAdmin):
    """ Bookmark 클래스가 Admin 사이트에서 어떤 모습으로 보여줄지를 정의하는 클래스이다. """
    list_display = ('title', 'url')

admin.site.register(Bookmark, BookmarkAdmin)

