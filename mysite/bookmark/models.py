from __future__ import unicode_literals  # Python2.x 지원 용, Python2.x 에서 Python3 문법을 사용하게 하기 위해

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.


@python_2_unicode_compatible
class Bookmark(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField('url', unique=True)

    def __str__(self):
        return self.title

