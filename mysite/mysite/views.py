from django.views.generic import TemplateView

from django.views.generic.edit import CreateView
# CreateView
# 테이블의 레코드를 생성하기 위해 필요한 폼을 보여주고 폼의 입력을 받아서 테이블의 레코드를 생성해주는 뷰

from django.contrib.auth.forms import UserCreationForm
# UserCreationForm
# User 모델의 객체를 생성하기 위해 보여주는 폼

from django.core.urlresolvers import reverse_lazy

# --- Homepage View
class HomeView(TemplateView):
    template_name = 'home.html'


# --- User Creation
class UserCreateView(CreateView):
    template_name = 'registration/register.html'
    form_class =UserCreationForm
    success_url = reverse_lazy('register_done')


class UserCreateDoneTV(TemplateView):
    template_name = 'registration/register_done.html'

